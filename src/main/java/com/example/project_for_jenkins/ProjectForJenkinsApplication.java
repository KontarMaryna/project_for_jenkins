package com.example.project_for_jenkins;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectForJenkinsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectForJenkinsApplication.class, args);
	}

}
